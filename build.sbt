import Resolvers._

ThisBuild / resolvers ++= allResolvers
ThisBuild / credentials ++= Seq(boxCredentials)

name := "boxever-realtime-analytics-etl"

version := "0.1-SNAPSHOT"

organization := "com.boxever"

ThisBuild / scalaVersion := "2.12.15" // Flink does not support newer Scala versions

lazy val flinkVersion = "1.13.2"
lazy val scalaTestVersion = "3.2.3"
lazy val boxeverV2EntitiesVersion = "0.0.207"
lazy val junitPlatformVersion="1.8.2"
lazy val awsJavaSdkVersion = "2.14.7"
lazy val jacksonVersion = "2.10.5"
lazy val monocleVersion = "2.0.4"

val flinkDependencies = Seq(
  "com.amazonaws" % "aws-kinesisanalytics-flink" % "2.0.0",
  "com.amazonaws" % "aws-kinesisanalytics-runtime" % "1.2.0",

  "org.apache.flink" %% "flink-clients" % flinkVersion, 
  "org.apache.flink" %% "flink-table-api-scala-bridge" % flinkVersion,
  "org.apache.flink" %% "flink-table-planner" % flinkVersion,
  "org.apache.flink" %% "flink-scala" % flinkVersion,
  "org.apache.flink" % "flink-connector-base" % flinkVersion,
  "org.apache.flink" %% "flink-streaming-scala" % flinkVersion,
  "org.apache.flink" %% "flink-connector-kafka" % flinkVersion,
  "org.apache.flink" %% "flink-connector-jdbc" % flinkVersion,

  "org.postgresql" % "postgresql" % "42.3.2",

  "com.boxever" % "boxever-v2-entity-models" % boxeverV2EntitiesVersion,
  "com.boxever" % "boxever-v2-entity-kafka" % boxeverV2EntitiesVersion,
  "com.fasterxml.jackson.core" % "jackson-databind" % "2.10.5.1",
  "com.fasterxml.jackson.core" % "jackson-core" % jacksonVersion,
  "com.fasterxml.jackson.core" % "jackson-annotations" % jacksonVersion,
  "com.fasterxml.jackson.datatype" % "jackson-datatype-jsr310" % jacksonVersion,
  "com.fasterxml.jackson.dataformat" % "jackson-dataformat-properties" % jacksonVersion,
  "com.fasterxml.jackson.module" %% "jackson-module-scala" % jacksonVersion,

  "com.twitter" %% "algebird-core" % "0.13.9",

  "software.amazon.awssdk" % "secretsmanager" % awsJavaSdkVersion,

  "com.typesafe.scala-logging" %% "scala-logging" % "3.9.4",



)

val testDeps = Seq(
  "org.mockito" %% "mockito-scala" % "1.16.23" % Test,
  "org.apache.flink" %% "flink-test-utils" % flinkVersion % Test,
  "org.apache.flink" %% "flink-runtime" % flinkVersion % Test,
  "org.apache.flink" %% "flink-streaming-java" % flinkVersion % Test classifier "tests",

  "org.scalactic" %% "scalactic" % scalaTestVersion % Test,
  "org.scalatest" %% "scalatest" % scalaTestVersion % Test,
  "org.scalatest" %% "scalatest-shouldmatchers" % scalaTestVersion % Test,
  // Must match flink kafka connectors kafka  client version
  "io.github.embeddedkafka" %% "embedded-kafka" % "2.4.1" % Test,
  "com.github.julien-truffaut" %%  "monocle-core"  % monocleVersion % Test,
  "com.github.julien-truffaut" %%  "monocle-macro" % monocleVersion % Test,

  // in order to filter test messages
  //https://stackoverflow.com/questions/29065603/complete-scala-logging-example
  "ch.qos.logback" % "logback-classic" % "1.2.11" % Test,
  "org.apache.commons" % "commons-lang3" % "3.11"
)


lazy val root = (project in file(".")).
  settings(
    libraryDependencies ++= flinkDependencies ++ testDeps,
    scalacOptions ++= Seq("-target:jvm-1.8"),
  )

assembly / mainClass := Some("com.boxever.analytics.Entry")
//assembly / assemblyJarName := "AnalyticsRT-123.jar"
// This is to make it work with scala

assemblyMergeStrategy in assembly := {
  case PathList("META-INF", xs @ _*) => MergeStrategy.discard
  // This seems relevant for JDK 9+ and will have to keep them if we use it
  case PathList("META-INF", "versions", "9", "module-info.class") => MergeStrategy.discard
  case "module-info.class" => MergeStrategy.discard
  case x =>
    val oldStrategy = (assemblyMergeStrategy in assembly).value
    oldStrategy(x)
}


// make run command include the provided dependencies
Compile / run  := Defaults.runTask(Compile / fullClasspath,
                                   Compile / run / mainClass,
                                   Compile / run / runner
                                  ).evaluated

// stays inside the sbt console when we press "ctrl-c" while a Flink programme executes with "run" or "runMain"
Compile / run / fork := true
Global / cancelable := true

// exclude Scala library from assembly
assembly / assemblyOption  := (assembly / assemblyOption).value.copy(includeScala = false)

// need to run test on assembly for jenkins
//test in assembly := {}

