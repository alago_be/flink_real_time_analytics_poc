package com.boxever.analytics.models

import com.boxever.kafka.model.v2.EntityModifiedDocumentMessage
import org.apache.flink.api.common.serialization.DeserializationSchema
import org.apache.flink.api.common.typeinfo.TypeInformation


class EMDV2DeserializationSchema[_ <: EntityModifiedDocumentMessage[_]] extends DeserializationSchema[EntityModifiedDocumentMessage[_]] {
  override def deserialize(message: Array[Byte]): EntityModifiedDocumentMessage[_] = {
      ObjectMapperV2.parse(message, classOf[EntityModifiedDocumentMessage[_]])
  }

  override def isEndOfStream(nextElement: EntityModifiedDocumentMessage[_]): Boolean = true

  override def getProducedType: TypeInformation[EntityModifiedDocumentMessage[_]] = TypeInformation.of(classOf[EntityModifiedDocumentMessage[_]])
}


