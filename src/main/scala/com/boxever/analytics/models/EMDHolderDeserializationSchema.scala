package com.boxever.analytics.models

import com.boxever.analytics.connectors.sources.{CampaignEMDHolder, EMDHolder, EventEMDHolder, GuestEMDHolder, OrderEMDHolder, SegmentEMDHolder, SessionEMDHolder}
import com.boxever.kafka.model.v2.BaseEntityMessage.EntityType
import com.boxever.kafka.model.v2.{EntityModifiedCampaignMessage, EntityModifiedDocumentMessage, EntityModifiedEventMessage, EntityModifiedGuestMessage, EntityModifiedOrderMessage, EntityModifiedSegmentDefinitionMessage, EntityModifiedSessionMessage}
import org.apache.flink.api.common.serialization.DeserializationSchema
import org.apache.flink.api.common.typeinfo.TypeInformation

class EMDHolderDeserializationSchema[_ <: EMDHolder] extends DeserializationSchema[EMDHolder] {
  override def deserialize(message: Array[Byte]): EMDHolder= {
    EMDHolderDeserializationSchema.deserialize(message)
  }

  override def isEndOfStream(nextElement: EMDHolder): Boolean = true

  override def getProducedType: TypeInformation[EMDHolder] = TypeInformation.of(classOf[EMDHolder])
}

object EMDHolderDeserializationSchema {
  def deserialize(message: Array[Byte]): EMDHolder = {
    val emd = ObjectMapperV2.parse(message, classOf[EntityModifiedDocumentMessage[_]])
    EntityType.valueOf(emd.getType) match {
      case EntityType.GUEST => GuestEMDHolder(emd.asInstanceOf[EntityModifiedGuestMessage])
      case EntityType.SESSION => SessionEMDHolder(emd.asInstanceOf[EntityModifiedSessionMessage])
      case EntityType.EVENT => EventEMDHolder(emd.asInstanceOf[EntityModifiedEventMessage])
      case EntityType.ORDER => OrderEMDHolder(emd.asInstanceOf[EntityModifiedOrderMessage])
      case EntityType.CAMPAIGN => CampaignEMDHolder(emd.asInstanceOf[EntityModifiedCampaignMessage])
      case EntityType.SEGMENT_DEFINITION => SegmentEMDHolder(emd.asInstanceOf[EntityModifiedSegmentDefinitionMessage])
    }
  }
}