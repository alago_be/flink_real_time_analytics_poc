package com.boxever.analytics.models

import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.core.`type`.TypeReference
import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.ObjectMapper

import java.util
// Code shamelessly copied and adapted from entity-merge-job project
// DO NOT USE FLINK SHADED JACKSON: IT FAILS TO GET SUBTYPES... took me a whole day to catch this :( ...
object ObjectMapperV2 {
  private val mapper = new ObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
    .configure(DeserializationFeature.READ_UNKNOWN_ENUM_VALUES_AS_NULL, true)
    .setSerializationInclusion(JsonInclude.Include.NON_EMPTY)

  def parse[T](s: String, returnType: Class[T]): T = try mapper.readValue(s, returnType)
  catch {
    case e: Exception =>
      throw new RuntimeException(e)
  }

  def parse[T](s: Array[Byte], returnType: Class[T]): T = try mapper.readValue(s, returnType)
  catch {
    case e: Exception =>
      throw new RuntimeException(e)
  }


  def serialize(o: Any): String = try mapper.writeValueAsString(o)
  catch {
    case e: Exception =>
      throw new RuntimeException(e)
  }
}
