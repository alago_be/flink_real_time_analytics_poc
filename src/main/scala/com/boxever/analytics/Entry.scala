package com.boxever.analytics

import com.amazonaws.services.kinesisanalytics.runtime.KinesisAnalyticsRuntime
import com.boxever.analytics.jobs.jobList
import com.typesafe.scalalogging.LazyLogging

object Entry extends LazyLogging {
  def main(args: Array[String]): Unit = {
    import collection.JavaConverters._

    val props = KinesisAnalyticsRuntime.getApplicationProperties
    logger.info(s"Running with pros: ${props}")

    val name = props.get("application").get("name")
    logger.info(s"running job ${props.get("application").get("name")}")

    val job = jobList(props.get("application").get("name").toString)(props.asScala.toMap)
    job.runJob(name.asInstanceOf[String])

  }

}
