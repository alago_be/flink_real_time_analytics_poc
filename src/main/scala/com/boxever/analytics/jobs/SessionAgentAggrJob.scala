/*
package com.boxever.analytics.jobs

import com.boxever.analytics.config.AggrJobConfig
import com.boxever.analytics.connectors.sinks.jdbc.JDBCSinkCreator
import com.boxever.analytics.connectors.sources.{EMDHolder, EMDSource}
import com.boxever.analytics.connectors.sources.EMDSource.ModifiedAtTimestampAssigner
import com.boxever.analytics.jobs.SessionAgentAggrJob.{DistinctSessionWindowFunction, SessionStatsAggregator, SessionStatsProcessWindow, createUniqueSessionSink, createUserAgentSink, newSessionsSideOutput}
import com.boxever.kafka.model.v2.BaseEntityMessage.EntityType
import com.boxever.kafka.model.v2.{EntityModifiedDocumentMessage, EntityModifiedSessionMessage}
import com.boxever.models.v2.Guest
import org.apache.flink.api.common.eventtime.WatermarkStrategy
import org.apache.flink.api.common.functions.AggregateFunction
import org.apache.flink.api.java.functions.KeySelector
import org.apache.flink.connector.kafka.source.KafkaSource
import org.apache.flink.streaming.api.functions.{KeyedProcessFunction, ProcessFunction}
import org.apache.flink.streaming.api.functions.sink.SinkFunction
import org.apache.flink.streaming.api.scala.OutputTag
import org.apache.flink.streaming.api.scala.function.ProcessWindowFunction
import org.apache.flink.streaming.api.windowing.assigners.TumblingEventTimeWindows
import org.apache.flink.streaming.api.windowing.time.Time
import org.apache.flink.streaming.api.windowing.windows.TimeWindow
import org.apache.flink.util.Collector

import java.sql.{PreparedStatement, Timestamp}

case class AggregatedSessionUserAgent(clientKey: String, userAgent: String, pointOfSale: String, count: Long, windowTime: Long)
case class AggregatedUniqueSession(clientKey: String, pointOfSale: String, count: Long, windowTime: Long)
case class SessionDimensionsSelector(clientKey: String, userAgent: String, pointOfSale: String)

class SessionAgentAggrJob(applicationConf: Map[String, String],
                          emd: SourceWithResultType[EMDHolder],
                          sessionUserAgentSink: => SinkFunction[AggregatedSessionUserAgent],
                          uniqueSessionsSink: => SinkFunction[AggregatedUniqueSession],
                         ) extends Job {

  def this(applicationConf: Map[String, String],
           emd: KafkaSource[EMDHolder], jdbc: JDBCSinkCreator) = this(
    applicationConf,
    emd,
    createUserAgentSink(jdbc),
    createUniqueSessionSink(jdbc),
  )
  def this(config: AggrJobConfig) = this(config.application, EMDSource(config.kafka),
    new JDBCSinkCreator(config.jdbc))

  import org.apache.flink.streaming.api.scala._

  val uniqueSessionsTag = OutputTag[EntityModifiedSessionMessage]("unique-sessions-output")

  implicit val emdType =  emd.getProducedType
  implicit val intType = createTypeInformation[Int]

  val keySelector: EMDHolder=> SessionDimensionsSelector = e => {
    val s: EntityModifiedSessionMessage = e.message.asInstanceOf[EntityModifiedSessionMessage]
    SessionDimensionsSelector(s.clientKey, s.getSession.getUserAgent, s.getSession.getPointOfSale)
  }

  //val env = StreamExecutionEnvironment.getExecutionEnvironment
  env.enableCheckpointing(500000)

  val wmStrategy: WatermarkStrategy[emdT] = WatermarkStrategy
    .forMonotonousTimestamps[emdT]()
    .withTimestampAssigner( _ => ModifiedAtTimestampAssigner )

  val sessionsStream = env.fromSource(emd, wmStrategy, "EMD")
    .filter( _.getType == EntityType.SESSION.toString)
    .map{ _.asInstanceOf[EntityModifiedSessionMessage]}
    .filter(_.getSession.getStatus == "CLOSED")
    // TODO: should move the side output to after windowing?
    .process(newSessionsSideOutput(uniqueSessionsTag))

  sessionsStream
    .keyBy(keySelector)
    .window(TumblingEventTimeWindows.of(Time.minutes(1)))
    .aggregate(new SessionStatsAggregator(), new SessionStatsProcessWindow() )
    .addSink(sessionUserAgentSink)

  sessionsStream
    .getSideOutput(uniqueSessionsTag)
    .keyBy(keySelector)
    .window(TumblingEventTimeWindows.of(Time.minutes(1))).process(new DistinctSessionWindowFunction() )
    .addSink(uniqueSessionsSink)
}

object SessionAgentAggrJob {
  /*
CREATE TABLE IF NOT EXISTS session_user_agent (
  clientKey VARCHAR ( 50 ),
  userAgent VARCHAR(50),
  pointOfSale VARCHAR(50),
  count bigint,
  windowTime timestamp
);
 */

  def createUserAgentSink(jdbc: JDBCSinkCreator) : SinkFunction[AggregatedSessionUserAgent] = {
    jdbc
      // clientKey: String, userAgent: String, pointOfSale: String, count: Long, windowTime: Long
      .setStatement("insert into session_user_agent (clientKey, userAgent, pointOfSale, count, windowTime) values (?, ?, ?, ?, ?)")
      .setStatementBuilder( (stm: PreparedStatement, aggr: AggregatedSessionUserAgent) => {
        stm.setString(1, aggr.clientKey)
        stm.setString(2, aggr.userAgent)
        stm.setString(3, aggr.pointOfSale)
        stm.setLong(4, aggr.count)
        stm.setTimestamp(5, new Timestamp(aggr.windowTime))
      }).get[AggregatedSessionUserAgent]
  }

  /*
CREATE TABLE IF NOT EXISTS session_unique_count (
clientKey VARCHAR ( 50 ),
pointOfSale VARCHAR(50),
count bigint,
windowTime timestamp
);
*/
  def createUniqueSessionSink(jdbc: JDBCSinkCreator) : SinkFunction[AggregatedUniqueSession] = {
    jdbc
      // clientKey: String, userAgent: String, pointOfSale: String, count: Long, windowTime: Long
      .setStatement("insert into session_unique_count (clientKey, pointOfSale, count, windowTime) values (?, ?, ?, ?)")
      .setStatementBuilder( (stm: PreparedStatement, aggr: AggregatedUniqueSession) => {
        stm.setString(1, aggr.clientKey)
        stm.setString(2, aggr.pointOfSale)
        stm.setLong(3, aggr.count)
        stm.setTimestamp(4, new Timestamp(aggr.windowTime))
      }).get[AggregatedUniqueSession]
  }


  // [IN, ACC, OUT]
  class SessionStatsAggregator extends AggregateFunction[EntityModifiedSessionMessage, Long, Long] {
    override def createAccumulator() = 0L

    override def add(value: EntityModifiedSessionMessage, accumulator: Long): Long = accumulator + 1L

    override def getResult(accumulator: Long) = accumulator

    override def merge(a: Long, b: Long) = a+ b
  }

  // [IN, OUT, KEY, W <: Window]
  class SessionStatsProcessWindow extends ProcessWindowFunction[Long, AggregatedSessionUserAgent, SessionDimensionsSelector, TimeWindow] {

    override def process(key: SessionDimensionsSelector, context: Context, sums: Iterable[Long], out: Collector[AggregatedSessionUserAgent]): Unit = {
      val sum = sums.iterator.next()
      out.collect( AggregatedSessionUserAgent(key.clientKey, key.userAgent, key.pointOfSale, sum, context.window.maxTimestamp()) )
    }
  }


  class DistinctSessionWindowFunction extends ProcessWindowFunction[EntityModifiedSessionMessage, AggregatedUniqueSession, SessionDimensionsSelector, TimeWindow] {

    // Keyed window so clientKey is already unique
    // Have also split the streams based on guest type
    // just need to put the refs in a Set -- Warning: memory usage?!
    // Check this: https://medium.com/riskified-technology/streaming-with-probabilistic-data-structures-why-how-b83b2adcd5d4

    override def process(key: SessionDimensionsSelector, context: Context, elements: Iterable[EntityModifiedSessionMessage], out: Collector[AggregatedUniqueSession]): Unit = {
      val value = elements.map(_.getRef).toSet
      out.collect(AggregatedUniqueSession(key.clientKey, key.pointOfSale, value.size, context.window.maxTimestamp()))
    }
  }

  def newSessionsSideOutput(tag: OutputTag[EntityModifiedSessionMessage]): ProcessFunction[EntityModifiedSessionMessage, EntityModifiedSessionMessage] =
    new ProcessFunction[EntityModifiedSessionMessage, EntityModifiedSessionMessage] {
      override def processElement(
                                   value: EntityModifiedSessionMessage,
                                   ctx: ProcessFunction[EntityModifiedSessionMessage, EntityModifiedSessionMessage]#Context,
                                   out: Collector[EntityModifiedSessionMessage]): Unit = {
        // emit data to regular output
        out.collect(value)

        // emit data to side output
        ctx.output(tag, value)
      }
    }

}

*/
