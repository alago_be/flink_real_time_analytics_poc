package com.boxever.analytics.jobs.session.stats

import org.apache.flink.streaming.api.scala.function.ProcessWindowFunction
import org.apache.flink.streaming.api.windowing.windows.TimeWindow
import org.apache.flink.util.Collector

class SessionStatsProcessWindow extends ProcessWindowFunction[AggregatedSessionStats, SessionsAggregatedStatsByDimension, SessionStatsDimensionsSelector, TimeWindow] {

  override def process(key: SessionStatsDimensionsSelector, context: Context, stats: Iterable[AggregatedSessionStats], out: Collector[SessionsAggregatedStatsByDimension]): Unit = {
    val sum = SessionStatsMonoid.combineAll(stats)
    out.collect( SessionsAggregatedStatsByDimension(
      sum,
      SessionStatsDimensionsSelector(key.clientKey, key.pointOfSale),
      context.window.maxTimestamp()) )
  }
}