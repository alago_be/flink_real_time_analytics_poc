package com.boxever.analytics.jobs.session.stats

import cats.kernel.Monoid
import com.boxever.analytics.connectors.sinks.MapExtensions

import scala.collection.mutable

/**
 * Base [[Monoid]] class for aggregating session stats
 */
object SessionStatsMonoid extends Monoid[AggregatedSessionStats] {
    override def empty: AggregatedSessionStats = AggregatedSessionStats(0L, 0L, 0L, 0L, mutable.Map(),
      mutable.Map(), mutable.Map(), mutable.Map())

    override def combine(x: AggregatedSessionStats, y: AggregatedSessionStats): AggregatedSessionStats =
      AggregatedSessionStats(
        closed              = x.closed + y.closed,
        bounced             = x.bounced + y.bounced,
        pageViews           = x.pageViews + y.pageViews,
        allSessionsDuration = x.allSessionsDuration + y.allSessionsDuration,
        userAgentHist       = x.userAgentHist.merged(y.userAgentHist),
        osHist              = x.osHist.merged(y.osHist),
        firstPageHist       = x.firstPageHist.merged(y.firstPageHist),
        refererHist        = x.refererHist.merged(y.refererHist)
      )
}
