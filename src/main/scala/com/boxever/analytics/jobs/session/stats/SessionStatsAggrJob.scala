package com.boxever.analytics.jobs.session.stats

import com.boxever.analytics.config.{AggrJobConfig, ApplicationConf}
import com.boxever.analytics.connectors.sinks.jdbc.{JDBCSinkCreator, histToPGJson, windowTimeFormat}
import com.boxever.analytics.connectors.sinks.HistType
import com.boxever.analytics.connectors.sources.EMDSource.getStrategy
import com.boxever.analytics.connectors.sources.{EMDHolder, EMDSource}
import com.boxever.analytics.jobs.session.stats.SessionStatsAggrJob.{createClosedSessionStatsSink, keySelector, mapEMDtoSessionStats}
import com.boxever.analytics.jobs.{Job, SourceWithResultType}
import com.boxever.analytics.prettyPrint
import com.boxever.kafka.model.v2.BaseEntityMessage.EntityType
import com.boxever.kafka.model.v2.EntityModifiedSessionMessage
import com.typesafe.scalalogging.LazyLogging
import org.apache.flink.api.common.typeinfo.TypeInformation
import org.apache.flink.connector.kafka.source.KafkaSource
import org.apache.flink.streaming.api.functions.sink.SinkFunction
import org.apache.flink.streaming.api.windowing.assigners.TumblingEventTimeWindows
import org.apache.flink.streaming.api.windowing.time.Time

import java.sql.{PreparedStatement, Timestamp}
import java.time.Duration
import scala.jdk.CollectionConverters.collectionAsScalaIterableConverter
import scala.util.Try


case class SessionClosedStats(  clientKey: String,
                                modifiedAt: Long,
                                emdType: String,
                                sessionRef: String,
                                createdAt: Long,
                                status: String,
                                pointOfSale: String,
                                channel: String,
                                operatingSystem: String,
                                userAgent: String,
                                startedAt: Long,
                                endedAt: Long,
                                numViews: Long,
                                firstPage: String,
                                referer: String
                             )
case class AggregatedSessionStats( closed: Long, bounced: Long, pageViews: Long, allSessionsDuration: Long, userAgentHist: HistType,
                                   osHist: HistType,/* deviceHist: HistType,*/ firstPageHist: HistType, refererHist: HistType
                                   )
case class SessionStatsDimensionsSelector(clientKey: String, pointOfSale: String)
case class SessionsAggregatedStatsByDimension( stats: AggregatedSessionStats, dimensions: SessionStatsDimensionsSelector, windowTime: Long )

/**
 *
 * @param applicationConf
 * @param emd a class complying with SourceWithResultType[EMDHolder] type
 * @param sessionStatsAgentSink a [[SinkFunction]] to store [[SessionsAggregatedStatsByDimension]]
 *
 * @see [[SourceWithResultType]]
 * @see [[EMDHolder]]
 */
class SessionStatsAggrJob(applicationConf: ApplicationConf, emd: SourceWithResultType[EMDHolder],
                          sessionStatsAgentSink: => SinkFunction[SessionsAggregatedStatsByDimension]
                         ) extends Job with LazyLogging {

  def this(applicationConf: ApplicationConf, emd: KafkaSource[EMDHolder], jdbc: JDBCSinkCreator) =
    this(applicationConf, emd, createClosedSessionStatsSink(jdbc, applicationConf.getProperties().get("schema_name"),
      applicationConf.getProperties().get("closed_sessions_table")))

  def this(config: AggrJobConfig) = this(config.application, EMDSource(config.kafka),
    new JDBCSinkCreator(config.jdbc))

  import org.apache.flink.api.scala._

  logger.info(s"Config properties: ${prettyPrint(applicationConf)}")

  implicit val emdType: TypeInformation[EMDHolder] = emd.getProducedType
  implicit val intType: TypeInformation[Int] = createTypeInformation[Int]

  val wmStrategy = getStrategy( applicationConf.getWaterMarkStrategy, applicationConf.getOutOfOrderness)

  env.enableCheckpointing(500000)

  env.fromSource(emd, wmStrategy.withIdleness(Duration.ofMinutes(applicationConf.getIdleness.toLong)), "EMD")
    .filter(_._type == EntityType.SESSION)
    .map(mapEMDtoSessionStats)
    .filter( _.status == "CLOSED" )
    .keyBy(keySelector)
    .window(TumblingEventTimeWindows.of(Time.minutes(1)))
    .aggregate(
      new SessionStatsAggregator(), new SessionStatsProcessWindow()
    )
    .addSink(sessionStatsAgentSink)

}


object SessionStatsAggrJob {

  val keySelector: SessionClosedStats => SessionStatsDimensionsSelector = s => {
    SessionStatsDimensionsSelector(s.clientKey, s.pointOfSale)
  }

  def createClosedSessionStatsSink(jdbc: JDBCSinkCreator, schema_name: String, table: String) : SinkFunction[SessionsAggregatedStatsByDimension] = {
    jdbc
      // clientKey: String, pointOfSale: String, closed: Long, bounced: Long, pageViews: Long,
      //                                  windowTime: Long
      .setStatement(
        s"""insert into ${schema_name}.${table}(
          |client_key,
          |time_window_minute,
          |point_of_sale,
          |segment_ref,
          |active_session_count,
          |bounced_session_count,
          |page_view_count,
          |session_duration,
          |session_operating_system_map,
          |session_firstpage_map,
          |session_referer_map,
          |session_device_map )
          |values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
          |""".stripMargin
      )
      .setStatementBuilder( (stm: PreparedStatement, aggr: SessionsAggregatedStatsByDimension) => {
        stm.setString(1, aggr.dimensions.clientKey)
        stm.setString(2, windowTimeFormat.format(new Timestamp(aggr.windowTime)))
        stm.setString(3, aggr.dimensions.pointOfSale)
        stm.setString(4, "NONE")
        stm.setLong(5, aggr.stats.closed)
        stm.setLong(6, aggr.stats.bounced)
        stm.setLong(7, aggr.stats.pageViews)
        stm.setDouble(8, aggr.stats.allSessionsDuration)
        stm.setObject(9, histToPGJson(aggr.stats.osHist))
        stm.setObject(10, histToPGJson(aggr.stats.firstPageHist))
        stm.setObject(11, histToPGJson(aggr.stats.refererHist))
        // UserAgent is Session Device
        stm.setObject(12, histToPGJson(aggr.stats.userAgentHist))

      }).get[SessionsAggregatedStatsByDimension]
  }


  def mapEMDtoSessionStats: EMDHolder => SessionClosedStats = { holder =>

    val emd = holder.message.asInstanceOf[EntityModifiedSessionMessage]
    val s = emd.getSession

    val numViews = if (s.getDataExtensions != null) {
      s.getDataExtensions.asScala.find(_.getName == "SessionStatistics").map(de =>
        de.getValues.getOrDefault("views", "0").toString.toLong
      ).getOrElse(0L)
    } else {
      0L
    }

    SessionClosedStats(
      emd.clientKey,
      Try{s.getModifiedAt.getTime}.getOrElse(0L),
      emd.getType,
      emd.getRef,
      Try{s.getCreatedAt.getTime}.getOrElse(0L),
      s.getStatus,
      s.getPointOfSale,
      s.getChannel,
      s.getOperatingSystem,
      s.getUserAgent,
      Try{s.getStartedAt.getTime}.getOrElse(0L),
      Try{s.getEndedAt.getTime}.getOrElse(0L),
      numViews,
      s.getFirstPageURI,
      s.getReferer
    )
  }

}

