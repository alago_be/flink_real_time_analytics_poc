package com.boxever.analytics.jobs.session.stats

import com.boxever.analytics.connectors.sinks.{HistType, MapExtensions}
import org.apache.flink.api.common.functions.AggregateFunction

class SessionStatsAggregator extends AggregateFunction[SessionClosedStats, AggregatedSessionStats, AggregatedSessionStats] {
  override def createAccumulator(): AggregatedSessionStats = SessionStatsMonoid.empty

  override def add(value: SessionClosedStats, accumulator: AggregatedSessionStats): AggregatedSessionStats = {
    val views = value.numViews
    val bounced = if( views == 1 ) 1 else 0
    val duration = value.endedAt - value.startedAt
    AggregatedSessionStats(
      closed              = accumulator.closed + 1,
      bounced             = accumulator.bounced + bounced,
      pageViews           = accumulator.pageViews + views ,
      allSessionsDuration = accumulator.allSessionsDuration + duration,
      userAgentHist       = accumulator.userAgentHist.inc(value.userAgent),
      osHist              = accumulator.osHist.inc(value.operatingSystem),
      firstPageHist       = accumulator.firstPageHist.inc(value.firstPage),
      refererHist         = accumulator.refererHist.inc(value.referer),
    )
  }

  override def getResult(accumulator: AggregatedSessionStats): AggregatedSessionStats = accumulator

  override def merge(a: AggregatedSessionStats, b: AggregatedSessionStats): AggregatedSessionStats = {
    SessionStatsMonoid.combine(a, b)
  }
}
