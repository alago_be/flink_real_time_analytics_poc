package com.boxever.analytics.jobs

import com.boxever.analytics.config.{AggrJobConfig, ApplicationConf}
import com.boxever.analytics.connectors.sinks.jdbc.JDBCSinkCreator
import org.apache.flink.streaming.api.scala.function.ProcessWindowFunction
import org.apache.flink.util.Collector
import com.boxever.analytics.connectors.sources
import com.boxever.analytics.connectors.sources.EMDSource.{ModifiedAtTimestampAssigner, ModifiedAtWatermarkGeneratorSupplier}
import com.boxever.analytics.connectors.sources.{EMDHolder, EMDSource}
import com.boxever.analytics.jobs.GuestsTypeVisitAggrJob.{DistinctGuestWindowFunction, createGuestsSink}
import com.boxever.kafka.model.v2.BaseEntityMessage.EntityType
import com.boxever.kafka.model.v2.{EntityModifiedDocumentMessage, EntityModifiedGuestMessage}
import com.typesafe.scalalogging.LazyLogging
import org.apache.flink.api.common.eventtime.WatermarkStrategy
import org.apache.flink.api.common.typeinfo.TypeInformation
import org.apache.flink.connector.kafka.source.KafkaSource
import org.apache.flink.streaming.api.functions.sink.SinkFunction
import org.apache.flink.streaming.api.windowing.assigners.TumblingEventTimeWindows
import org.apache.flink.streaming.api.windowing.time.Time
import org.apache.flink.streaming.api.windowing.windows.TimeWindow
import org.apache.flink.streaming.api.scala._

import java.sql.{PreparedStatement, Timestamp}


case class DistinctGuestPerOrg( clientKey: String, guestType: String, count: Long, windowTime: Long )
case class OrgGuestTypeKey(clientKey: String, guestType: String)

class GuestsTypeVisitAggrJob(applicationConf:ApplicationConf,
                             emd: SourceWithResultType[EMDHolder],
                             guestsSink: => SinkFunction[DistinctGuestPerOrg]
  ) extends Job {

  def this(applicationConf: ApplicationConf, emd: KafkaSource[EMDHolder], jdbc: JDBCSinkCreator) =
    this(applicationConf, emd, createGuestsSink(jdbc))

  def this(config: AggrJobConfig) = this(config.application, EMDSource(config.kafka),
    new JDBCSinkCreator(config.jdbc))

  implicit val guestKey: EntityModifiedGuestMessage => OrgGuestTypeKey = g => {
    OrgGuestTypeKey(g.clientKey, g.getEntity.getGuestType)
  }


  implicit val emdType: TypeInformation[EMDHolder] =  emd.getProducedType
  implicit val stringType: TypeInformation[String] = createTypeInformation[String]
  implicit val longType:  TypeInformation[Long] = createTypeInformation[Long]

  env.registerType(classOf[EntityModifiedGuestMessage])
  env.enableCheckpointing(5000)
  //val customerTypeSplitStreamProcessor = new CustomerTypeSplitStreamProcessor(visitorOutputTag)

  val wmStrategy: WatermarkStrategy[EMDHolder] = WatermarkStrategy
    .forGenerator(new ModifiedAtWatermarkGeneratorSupplier(3500L))
    .withTimestampAssigner( _ => ModifiedAtTimestampAssigner )

  env
    .fromSource(emd, wmStrategy, "EMD")
    .filter( _._type == EntityType.GUEST )
    .map(_.message.asInstanceOf[EntityModifiedGuestMessage])
    .keyBy(guestKey)
    .window(TumblingEventTimeWindows.of(Time.minutes(1)))
    .process[DistinctGuestPerOrg](new DistinctGuestWindowFunction())
    .addSink(guestsSink)

}


object GuestsTypeVisitAggrJob extends LazyLogging {

  class DistinctGuestWindowFunction extends ProcessWindowFunction[EntityModifiedGuestMessage, DistinctGuestPerOrg, OrgGuestTypeKey, TimeWindow] {

    // Keyed window so clientKey is already unique
    // Have also split the streams based on guest type
    // just need to put the refs in a Set -- Warning: memory usage?!
    // Check this: https://medium.com/riskified-technology/streaming-with-probabilistic-data-structures-why-how-b83b2adcd5d4

    override def process(key: OrgGuestTypeKey, context: Context, elements: Iterable[EntityModifiedGuestMessage], out: Collector[DistinctGuestPerOrg]): Unit = {
      logger.info(s"ITEMS: ${elements.size} time: ${context.window.maxTimestamp}")
      val value = elements.map(_.getRef).toSet
      out.collect(DistinctGuestPerOrg(key.clientKey, key.guestType, value.size, context.window.maxTimestamp()))
    }
  }


  /*
CREATE TABLE IF NOT EXISTS guests_count (
  windowTime timestamp,
  count bigint,
  clientKey VARCHAR ( 50 ),
  guestType VARCHAR(10)
);
 */

  def createGuestsSink(jdbc: JDBCSinkCreator) : SinkFunction[DistinctGuestPerOrg] = {
    jdbc
      .setStatement("insert into guests_count (windowTime, count, clientKey, guestType) values (?, ?, ?, ?)")
      .setStatementBuilder( (stm: PreparedStatement, aggr: DistinctGuestPerOrg) => {
        stm.setTimestamp(1, new Timestamp(aggr.windowTime))
        stm.setLong(2, aggr.count)
        stm.setString(3, aggr.clientKey)
        stm.setString(4, aggr.guestType)
      }).get[DistinctGuestPerOrg]
  }

}
