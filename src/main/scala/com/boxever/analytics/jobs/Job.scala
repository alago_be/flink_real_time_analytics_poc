package com.boxever.analytics.jobs

import org.apache.flink.api.common.JobExecutionResult
import org.apache.flink.streaming.api.scala.StreamExecutionEnvironment

trait Job {
  val env: StreamExecutionEnvironment = StreamExecutionEnvironment.getExecutionEnvironment
  def runJob(name: String): JobExecutionResult = {
    // execute program
    env.execute(name)
  }
}

object Job {

}