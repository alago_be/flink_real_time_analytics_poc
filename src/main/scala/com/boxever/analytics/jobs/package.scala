package com.boxever.analytics

import com.boxever.analytics.config.{AggrJobConfig, ApplicationConf}
import com.boxever.analytics.connectors.sinks.jdbc.JDBCConnectionConfig
import com.boxever.analytics.connectors.sources.KafkaConfig
import com.boxever.analytics.jobs.session.stats.SessionStatsAggrJob

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.dataformat.javaprop.JavaPropsMapper
import com.fasterxml.jackson.module.scala.{DefaultScalaModule, ScalaObjectMapper}
import org.apache.flink.api.connector.source.{Source, SourceSplit}
import org.apache.flink.api.java.typeutils.ResultTypeQueryable

import java.util.Properties
import scala.language.implicitConversions

package object jobs {

  /**
   * Type with same traits than [[org.apache.flink.connector.kafka.source.KafkaSource]] for easier testing
   *
   * @tparam T Result Type
   */
  type SourceWithResultType[T] = Source[T,_<:SourceSplit,_] with ResultTypeQueryable[T]

  implicit def propsToConfigMapper(props: Map[String, Properties]): AggrJobConfig = {
    val propsMapper: JavaPropsMapper = new JavaPropsMapper
    val scalaMapper = new ObjectMapper() with ScalaObjectMapper
    scalaMapper.registerModule(DefaultScalaModule)
    AggrJobConfig(
      scalaMapper.convertValue(props("application"), classOf[ApplicationConf]),
      propsMapper.convertValue(props("kafka"), classOf[KafkaConfig]),
      propsMapper.convertValue(props("jdbc"), classOf[JDBCConnectionConfig])
    )
  }


  /**
   * Map holding all the available Jobs to instantiate on initialization
   */
  val jobList: Map[String, Map[String, Properties] => Job] = Map(
    "GuestsTypeVisitAggrJob" -> (c => new GuestsTypeVisitAggrJob(c)),
    //"SessionsUserAgentJob" -> (c => new SessionAgentAggrJob(c)),
    "SessionStatsAggrJob" -> (c => new SessionStatsAggrJob(c))
  )

}
