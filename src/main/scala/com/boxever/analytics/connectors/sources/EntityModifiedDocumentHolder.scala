package com.boxever.analytics.connectors.sources

import com.boxever.kafka.model.v2.BaseEntityMessage.EntityType
import com.boxever.kafka.model.v2.{BaseEntityMessage, EntityModifiedCampaignMessage, EntityModifiedEventMessage, EntityModifiedGuestMessage, EntityModifiedOrderMessage, EntityModifiedSegmentDefinitionMessage, EntityModifiedSessionMessage}
import org.apache.flink.api.common.typeinfo.TypeInformation

import scala.language.reflectiveCalls
import scala.reflect.ClassTag

/**
 * Basic trait that will hold a EMD message of a given type T
 */
trait EMDHolder {
  type T
  val _type: EntityType
  val message: T
}

final case class GuestEMDHolder(override val message: EntityModifiedGuestMessage,
                                override val _type: EntityType = EntityType.GUEST) extends EMDHolder {type T = EntityModifiedGuestMessage}
final case class OrderEMDHolder(override val message: EntityModifiedOrderMessage,
                                override val _type: EntityType = EntityType.ORDER) extends EMDHolder {type T = EntityModifiedOrderMessage}
final case class EventEMDHolder(override val message: EntityModifiedEventMessage,
                                override val _type: EntityType = EntityType.EVENT) extends EMDHolder {type T = EntityModifiedEventMessage}
final case class SessionEMDHolder(override val message: EntityModifiedSessionMessage,
                                  override val _type: EntityType = EntityType.SESSION) extends EMDHolder {type T = EntityModifiedSessionMessage}
final case class CampaignEMDHolder(override val message: EntityModifiedCampaignMessage,
                                   override val _type: EntityType = EntityType.CAMPAIGN) extends EMDHolder {type T = EntityModifiedCampaignMessage}
final case class SegmentEMDHolder(override val message: EntityModifiedSegmentDefinitionMessage,
                                  override val _type: EntityType = EntityType.SEGMENT_DEFINITION) extends EMDHolder {type T = EntityModifiedSegmentDefinitionMessage}

