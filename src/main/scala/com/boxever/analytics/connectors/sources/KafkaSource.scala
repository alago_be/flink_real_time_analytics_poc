package com.boxever.analytics.connectors.sources

import com.boxever.analytics.models.{EMDHolderDeserializationSchema, EMDV2DeserializationSchema}
import com.boxever.kafka.model.v2.EntityModifiedDocumentMessage
import com.boxever.models.v2.Entity
import com.fasterxml.jackson.annotation.{JsonCreator, JsonIgnoreProperties, JsonProperty}
import org.apache.flink.api.common.eventtime.{TimestampAssigner, TimestampAssignerSupplier, Watermark, WatermarkGenerator, WatermarkGeneratorSupplier, WatermarkOutput, WatermarkStrategy}
import org.apache.flink.connector.kafka.source.KafkaSource
import org.apache.flink.connector.kafka.source.enumerator.initializer.OffsetsInitializer

import scala.beans.BeanProperty
import scala.math

@JsonIgnoreProperties(ignoreUnknown = false)
@JsonCreator
case class KafkaConfig(@JsonProperty(value = "bootstrap", required = false) @BeanProperty bootstrap: String,
                       @JsonProperty(value = "topic", required = false)     @BeanProperty topic: String,
                       @JsonProperty(value = "groupID", required = false)   @BeanProperty groupID: String,
                       @JsonProperty(value = "offset", required = false)    @BeanProperty offset: String)

object EMDSource {

  /**
   * Creates a Kafka Source that deserialises emd topic messages to [[EMDHolder]] and its corresponding subclasses
   *
   * @param config of type [[KafkaConfig]]
   * @return [[org.apache.flink.connector.kafka.source.KafkaSource]]
   */
  def apply(config: KafkaConfig ): KafkaSource[EMDHolder] = {
    KafkaSource.builder[EMDHolder]()
      .setBootstrapServers(config.bootstrap)
      .setTopics(config.topic)
      .setGroupId(config.groupID)
      .setStartingOffsets(fromOffset(config.offset))
      .setValueOnlyDeserializer(new EMDHolderDeserializationSchema())
      .build()
  }



  /**
   * Helper class to get [[OffsetsInitializer]] from String
   *
   * @param offset name of offset type as string
   * @return the [[OffsetsInitializer]] requested
   */
  def fromOffset(offset: String): OffsetsInitializer = {
    // TODO: Improve
    offset match {
      case "earliest" => OffsetsInitializer.earliest()
      case "latest" => OffsetsInitializer.latest()
      case _ => OffsetsInitializer.earliest()
    }
  }

  /**
   * Helper method to return different [[WatermarkStrategy]], allowing to pass this through initialization configuration
   *
   * @param strategy Name of the [[WatermarkStrategy]]
   * @param outOfOrderness value of latency only used for OutOfOrderness Strategy
   * @return
   */
  def getStrategy(strategy: String, outOfOrderness: Long = 3500L): WatermarkStrategy[EMDHolder] = strategy match {
    case "Monotonous" =>  WatermarkStrategy.forMonotonousTimestamps()
      .withTimestampAssigner( _ => ModifiedAtTimestampAssigner )
    case "OutOfOrderness" =>  WatermarkStrategy.forGenerator(new ModifiedAtWatermarkGeneratorSupplier(outOfOrderness))
      .withTimestampAssigner( _ => ModifiedAtTimestampAssigner )
    case _ => WatermarkStrategy.forGenerator(new ModifiedAtWatermarkGeneratorSupplier(outOfOrderness))
      .withTimestampAssigner( _ => ModifiedAtTimestampAssigner )
  }

  /**
   * This generator generates watermarks assuming that elements arrive out of order,
   * but only to a certain degree. The latest elements for a certain timestamp t will arrive
   * at most n milliseconds after the earliest elements for timestamp t.
   */
  class BoundedOutOfOrdernessModifiedAtGenerator(val maxOutOfOrderness: Long)extends WatermarkGenerator[EMDHolder] {

    //val maxOutOfOrderness = 3500L // 3.5 seconds

    var currentMaxTimestamp: Long = _

    override def onEvent(element: EMDHolder, eventTimestamp: Long, output: WatermarkOutput): Unit = {
      val entity = element.message.asInstanceOf[EntityModifiedDocumentMessage[_]].getEntity
      if (entity.getModifiedAt() != null) {
        currentMaxTimestamp = entity.getModifiedAt.getTime
      } else {
        currentMaxTimestamp =  entity.getCreatedAt.getTime
      }
    }

    override def onPeriodicEmit(output: WatermarkOutput): Unit = {
      // emit the watermark as current highest timestamp minus the out-of-orderness bound
      output.emitWatermark(new Watermark(currentMaxTimestamp - maxOutOfOrderness - 1))
    }
  }

  class ModifiedAtWatermarkGeneratorSupplier(val maxOutOfOrderness: Long) extends WatermarkGeneratorSupplier[EMDHolder] {
    override def createWatermarkGenerator(context: WatermarkGeneratorSupplier.Context): WatermarkGenerator[EMDHolder] =
      new BoundedOutOfOrdernessModifiedAtGenerator(maxOutOfOrderness)
  }

  object ModifiedAtTimestampAssigner extends TimestampAssigner[EMDHolder] with Serializable {
    override def extractTimestamp(element: EMDHolder, recordTimestamp: Long): Long =
      element.message.asInstanceOf[EntityModifiedDocumentMessage[_]].getEntity.asInstanceOf[Entity].getModifiedAt.getTime
  }

}
