package com.boxever.analytics.connectors.sinks.jdbc

import com.boxever.analytics.connectors.sinks.SinkCreator
import com.boxever.analytics.connectors.sinks.jdbc.JDBCSinkCreator.{createStatementBuilder, getJdbcConnectionOptions}
import com.fasterxml.jackson.annotation.{JsonCreator, JsonIgnoreProperties, JsonProperty}
import com.fasterxml.jackson.databind.ObjectMapper
import com.typesafe.scalalogging.LazyLogging
import org.apache.flink.connector.jdbc.{JdbcConnectionOptions, JdbcExecutionOptions, JdbcSink, JdbcStatementBuilder}
import org.apache.flink.streaming.api.functions.sink.SinkFunction
import software.amazon.awssdk.http.apache.ApacheHttpClient
import software.amazon.awssdk.regions.Region
import software.amazon.awssdk.services.secretsmanager.SecretsManagerClient
import software.amazon.awssdk.services.secretsmanager.model.GetSecretValueRequest

import java.sql.PreparedStatement
import scala.beans.BeanProperty
import scala.reflect.ClassTag


@JsonIgnoreProperties(ignoreUnknown = false)
@JsonCreator
case class JDBCConnectionConfig(  @JsonProperty(value = "secret", required = false) @BeanProperty secret: String,
                                  @JsonProperty(value = "region", required = false) @BeanProperty region: String)

// JdbcConnectonOptions:
// JdbcExecutionOptions.builder()
// .withBatchIntervalMs(200)             // optional: default = 0, meaning no time-based execution is done
// .withBatchSize(1000)                  // optional: default = 5000 values
// .withMaxRetries(5)                    // optional: default = 3
// .build()
@JsonIgnoreProperties(ignoreUnknown = false)
@JsonCreator
case class DatabaseSecret(@JsonProperty(value = "password", required = false) @BeanProperty password: String,
                          @JsonProperty(value = "dbname", required = false) @BeanProperty dbname: String,
                          @JsonProperty(value = "engine", required = false) @BeanProperty engine: String,
                          @JsonProperty(value = "port", required = false) @BeanProperty port: String,
                          @JsonProperty(value = "dbInstanceIdentifier", required = false) @BeanProperty dbInstanceIdentifier: String,
                          @JsonProperty(value = "host", required = false) @BeanProperty host: String,
                          @JsonProperty(value = "username", required = false) @BeanProperty username: String
                         )

class JDBCSinkCreator(config: JDBCConnectionConfig,
                   jdbcExecutionOptions: JdbcExecutionOptions = JdbcExecutionOptions.builder().build()) extends SinkCreator[JDBCSinkCreator] {

  type Result = JDBCSinkCreator

  val connection = getJdbcConnectionOptions(config)

  var statement: Option[String] = None
  var statementBuilder: Option[JdbcStatementBuilder[_]] = None
  def setStatement(statement: String): JDBCSinkCreator = {
    this.statement = Some(statement)
    this
  }
  def setStatementBuilder(statementBuilder: JdbcStatementBuilder[_]): JDBCSinkCreator = {
    this.statementBuilder = Option.apply(statementBuilder)
    this
  }
  def setStatementBuilder(stm: (PreparedStatement, _) => Unit): JDBCSinkCreator = {
    this.setStatementBuilder( createStatementBuilder(stm) )
  }

  def get[A]()(implicit ev: ClassTag[A]): SinkFunction[A] = {
    if( statement.isEmpty || statementBuilder.isEmpty) {
      throw new RuntimeException("Error creating Sink Connector: data missing")
    }

    // I could probably avoid propagating class Tag, but will be back to this later ...
    JdbcSink.sink(statement.get, statementBuilder.get.asInstanceOf[JdbcStatementBuilder[A]], jdbcExecutionOptions, connection )
  }

}

object JDBCSinkCreator extends LazyLogging {

  def createStatementBuilder[T](method: (PreparedStatement, T) => Unit)(implicit ev: ClassTag[T]): JdbcStatementBuilder[T] =
    new JdbcStatementBuilder[T] {
      override def accept(ps: PreparedStatement, t: T): Unit = method(ps, t)
    }


  def getDatabaseSecret(config: JDBCConnectionConfig ): DatabaseSecret = {
    val region = Region.of(config.region)
    val secretName = config.secret

    // https://github.com/aws/aws-sdk-java-v2/issues/446
    val secretsClient: SecretsManagerClient = SecretsManagerClient.builder
      .region(region)
      .httpClientBuilder(ApacheHttpClient.builder()).build
    try {
        val valueRequest = GetSecretValueRequest.builder()
          .secretId(secretName)
          .build()

        val valueResponse = secretsClient.getSecretValue(valueRequest).secretString()

        val mapper = new ObjectMapper()
        mapper.readValue(valueResponse, classOf[DatabaseSecret])
      } catch {
          // TODO: Log
        case e: Exception=> throw e

      }
    }

  def getJdbcConnectionOptions(config: JDBCConnectionConfig): JdbcConnectionOptions = {
      val secret = getDatabaseSecret(config)
      // jdbc:postgresql://analytics-real-time-db-dev-eu-west-1.ccqkjxl0nl6a.eu-west-1.rds.amazonaws.com:5432/analyticsrtdb
      // URL Connection String	jdbc:postgresql:// server-name : server-port / database-name
      val url = s"jdbc:${getProtocol(secret.engine)}://${secret.host}:${secret.port}/${secret.dbname}"

      logger.info(s"DB URL: ${url}")
      new JdbcConnectionOptions.JdbcConnectionOptionsBuilder()
        .withUrl(url)
        // TODO: get from the secret
        .withDriverName(getDriver(secret.engine))
        .withUsername(secret.username)
        .withPassword(secret.password)
        .build()
    }

  def getDriver(engine: String): String = engine match {
    case "postgres" => "org.postgresql.Driver"
    case _ => "org.postgresql.Driver"
  }

  def getProtocol(engine: String): String = engine match {
    case "postgres" => "postgresql"
    case _ => "postgresql"
  }

}
