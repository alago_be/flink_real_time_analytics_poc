package com.boxever.analytics.connectors


import org.apache.flink.streaming.api.functions.sink.SinkFunction

import scala.collection.mutable
import scala.reflect.ClassTag

package object sinks {

  type HistType = mutable.Map[String, Long]

  trait SinkCreator[T <: SinkCreator[T]] {
    def get[A]()(implicit ev: ClassTag[A]): SinkFunction[A]
  }

  implicit class MapExtensions(map: HistType) {
    def inc(key: String): HistType = {
      if (key == null) {
        map
      } else {
        map(key) = map.getOrElse(key, 0L) + 1L
        map
      }
    }

    def add(key: String, value: Long): HistType = {
      if (key == null) {
        map
      } else {
        map(key) = map.getOrElse(key, 0L) + value
        map
      }
    }

    def merged(another: HistType): HistType = {
      val newMap = map.clone()
      another.foreach { case (k, v) =>
        newMap.add(k, v)
      }
      newMap
    }
  }

}
