package com.boxever.analytics.connectors.sinks

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.scala.{DefaultScalaModule, ScalaObjectMapper}
import org.postgresql.util.PGobject

import scala.collection.mutable

package object jdbc {

  val windowTimeFormat = new java.text.SimpleDateFormat("yyyy-MM-dd'T'HH:mm:'00'")

  val mapper = new ObjectMapper() with ScalaObjectMapper
  mapper.registerModule(DefaultScalaModule)

  def histToPGJson(map: HistType):  PGobject = {
    val jsonObject = new PGobject
    jsonObject.setType("json")
    jsonObject.setValue(mapper.writeValueAsString(map))
    jsonObject
  }
}
