package com.boxever.analytics.config

import com.boxever.analytics.connectors.sinks.jdbc.JDBCConnectionConfig
import com.boxever.analytics.connectors.sources.KafkaConfig
import com.fasterxml.jackson.annotation.{JsonCreator, JsonIgnoreProperties, JsonProperty}

import scala.beans.BeanProperty

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonCreator
case class AggrJobConfig(@JsonProperty(value = "application", required = false)  @BeanProperty application: ApplicationConf,
                         @JsonProperty(value = "kafka", required = false)        @BeanProperty kafka: KafkaConfig,
                         @JsonProperty(value = "jdbc", required = false)         @BeanProperty jdbc: JDBCConnectionConfig
                                       ) extends JobConfig