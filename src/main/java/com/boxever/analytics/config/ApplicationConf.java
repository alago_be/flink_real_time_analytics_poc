package com.boxever.analytics.config;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.HashMap;
import java.util.Map;

public class ApplicationConf {
    private Map<String, String> properties;
    private String name;
    private Integer idleness = 2;
    private Long outOfOrderness = 3500L;
    private String waterMarkStrategy = "OutOfOrderness";

    public ApplicationConf(){
        properties = new HashMap<>();
    }
    @JsonAnyGetter
    public Map<String, String> getProperties(){
        return properties;
    }

    @JsonAnySetter
    public void add(String property, String value){
        properties.put(property, value);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getIdleness() {
        return idleness;
    }

    public void setIdleness(Integer idleness) {
        this.idleness = idleness;
    }

    public Long getOutOfOrderness() {
        return outOfOrderness;
    }

    public void setOutOfOrderness(Long outOfOrderness) {
        this.outOfOrderness = outOfOrderness;
    }

    public String getWaterMarkStrategy() {
        return waterMarkStrategy;
    }

    public void setWaterMarkStrategy(String waterMarkStrategy) {
        this.waterMarkStrategy = waterMarkStrategy;
    }

    @Override
    public String toString() {
        try {
            ObjectMapper mapper = new ObjectMapper();
            return mapper.writeValueAsString(this);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }
}

