package com.boxever.analytics

import com.boxever.analytics.config.ApplicationConf
import com.boxever.analytics.connectors.sources.{EMDFileFormat, EMDHolder}
import com.boxever.analytics.connectors.sources.EMDSource.{ModifiedAtTimestampAssigner, ModifiedAtWatermarkGeneratorSupplier}
import com.boxever.analytics.jobs.session.stats.SessionStatsAggrJob.mapEMDtoSessionStats
import com.boxever.analytics.jobs.{DistinctGuestPerOrg, GuestsTypeVisitAggrJob}
import TestHelpers.{AggregatedByName, SimpleAggrFunction, SimpleProcessWindow}
import com.boxever.analytics.jobs.session.stats.{SessionStatsAggrJob, SessionsAggregatedStatsByDimension}
import com.boxever.kafka.model.v2.BaseEntityMessage.EntityType
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.scala.{DefaultScalaModule, ScalaObjectMapper}
import org.apache.flink.api.common.eventtime.WatermarkStrategy
import org.apache.flink.api.common.typeinfo.TypeInformation
import org.apache.flink.api.scala.createTypeInformation
import org.apache.flink.connector.file.src.FileSource
import org.apache.flink.core.fs.Path
import org.apache.flink.streaming.api.functions.sink.SinkFunction
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers
import org.apache.flink.runtime.testutils.MiniClusterResourceConfiguration
import org.apache.flink.streaming.api.scala.StreamExecutionEnvironment
import org.apache.flink.streaming.api.windowing.assigners.TumblingEventTimeWindows
import org.apache.flink.streaming.api.windowing.time.Time
import org.apache.flink.test.util.MiniClusterWithClientResource
import org.scalatest.BeforeAndAfter

import java.util.concurrent.ConcurrentLinkedQueue



class GuestsAggregationTest extends AnyFlatSpec with Matchers with BeforeAndAfter{

  val flinkCluster = new MiniClusterWithClientResource(new MiniClusterResourceConfiguration.Builder()
    .setNumberSlotsPerTaskManager(2)
    .setNumberTaskManagers(1)
    .build)

  before {
    flinkCluster.before()
  }

  after {
    flinkCluster.after()
  }

  // Some dummy testes need to be imrpoved

    "GuestsAggregation" should
      "work" in {

        val file = getClass.getResource("/data/emd_23_12_2021_dev_full.txt")
        val kafkaSource = FileSource.forRecordFileFormat(new EMDFileFormat(), new Path(file.getPath)).build()
        val jdbcSink = GuestsInMemorySinkFunction
        val appConf = new ApplicationConf()
        appConf.setName("TestApp")
        appConf.setIdleness(2)

        val job = new GuestsTypeVisitAggrJob(appConf, kafkaSource, jdbcSink)

        //job.runJob("GuestsCount")
      println(job.env.getExecutionPlan)
      job.env.execute("GuestsCount")

      jdbcSink.data.forEach(println)
      jdbcSink.data.size() should be(1212)
      }

  "multiple entities job" should
    "serialise emd inside holders" in {

    val file = getClass.getResource("/data/emd_23_12_2021_dev_full.txt")
    val kafkaSource = FileSource.forRecordFileFormat(new EMDFileFormat(), new Path(file.getPath)).build()
    val jdbcSink = MixedInMemorySinkFunction

    val env: StreamExecutionEnvironment = StreamExecutionEnvironment.getExecutionEnvironment
    val wmStrategy: WatermarkStrategy[EMDHolder] = WatermarkStrategy
      .forGenerator(new ModifiedAtWatermarkGeneratorSupplier(3500L))
      .withTimestampAssigner( _ => ModifiedAtTimestampAssigner )
    implicit val emdType = kafkaSource.getProducedType
    implicit val stringType = TypeInformation.of(classOf[String])


    env
      .fromSource(kafkaSource, wmStrategy, "EMD")
      .filter( x => (x._type == EntityType.GUEST || x._type == EntityType.SESSION))
      .keyBy( _._type.name())
      .window(TumblingEventTimeWindows.of(Time.minutes(20)))
      .aggregate( new SimpleAggrFunction(), new SimpleProcessWindow())
      .addSink(jdbcSink)
    env.execute("Test")
    jdbcSink.data.forEach(println)
    jdbcSink.data.size() should be(201)
  }
  "write out" should "serialise sessions to Case Class" in {

    val file = getClass.getResource("/data/emd_23_12_2021_dev_full.txt")
    val kafkaSource = FileSource.forRecordFileFormat(new EMDFileFormat(), new Path(file.getPath)).build()
    val jdbcSink = MixedInMemorySinkFunction

    val env: StreamExecutionEnvironment = StreamExecutionEnvironment.getExecutionEnvironment
    val wmStrategy: WatermarkStrategy[EMDHolder] = WatermarkStrategy
      .forGenerator(new ModifiedAtWatermarkGeneratorSupplier(3500L))
      .withTimestampAssigner( _ => ModifiedAtTimestampAssigner )
    implicit val emdType = kafkaSource.getProducedType
    implicit val stringType = TypeInformation.of(classOf[String])


    env
      .fromSource(kafkaSource, wmStrategy, "EMD")
      .filter( x => (x._type == EntityType.SESSION ))
      .map(mapEMDtoSessionStats)
      .filter(_.status=="CLOSED")
      .print("Test")
    env.execute("Test")

  }

  "Job" should
    "serialise emd inside holders" in {

    val file = getClass.getResource("/data/emd_23_12_2021_dev_full.txt")
    val kafkaSource = FileSource.forRecordFileFormat(new EMDFileFormat(), new Path(file.getPath)).build()
    val jdbcSink = GuestsInMemorySinkFunction
    val appConf = new ApplicationConf()
    appConf.setName("TestApp")
    appConf.setIdleness(2)
    val job = new GuestsTypeVisitAggrJob(appConf, kafkaSource, jdbcSink)

    //job.runJob("GuestsCount")
    println(job.env.getExecutionPlan)
    job.env.execute("GuestsCount")

    jdbcSink.data.forEach(println)
    jdbcSink.data.size() should be(2424)
  }

  "Sessions Job" should
    "serialise emd inside holders" in {

    val file = getClass.getResource("/data/emd_23_12_2021_dev_full.txt")
    val kafkaSource = FileSource.forRecordFileFormat(new EMDFileFormat(), new Path(file.getPath)).build()
    val jdbcSink = SessionsInMemorySinkFunction
    val appConf = new ApplicationConf()
    appConf.setName("TestApp")
    appConf.setIdleness(2)
    appConf.add("schema_name","Schema")
    appConf.add("closed_sessions_table","closed_sessions_table")

    val job = new SessionStatsAggrJob(appConf, kafkaSource, jdbcSink)

    //job.runJob("GuestsCount")
    val mapper = new ObjectMapper() with ScalaObjectMapper
    mapper.registerModule(DefaultScalaModule)

    println(job.env.getExecutionPlan)
    job.env.execute("SessionStatsJob")

    jdbcSink.data.forEach(m => {
      println(m.stats.userAgentHist.toMap)
      println(mapper.writeValueAsString(m.stats.userAgentHist.toMap))
      println(m)
    })
    jdbcSink.data.size() should be(65)
  }

}
// https://github.com/apache/flink/blob/770f2f83a81b2810aff171b2f56390ef686f725a/flink-streaming-connectors/flink-connector-kafka-0.8/src/test/java/org/apache/flink/streaming/connectors/kafka/KafkaTestEnvironmentImpl.java#L358
object GuestsInMemorySinkFunction extends SinkFunction[DistinctGuestPerOrg] {
  val data: ConcurrentLinkedQueue[DistinctGuestPerOrg] = new ConcurrentLinkedQueue[DistinctGuestPerOrg]()

  override def invoke(value: DistinctGuestPerOrg, context: SinkFunction.Context): Unit = {
    data.add(value)
  }
}

object SessionsInMemorySinkFunction extends SinkFunction[SessionsAggregatedStatsByDimension] {
  val data: ConcurrentLinkedQueue[SessionsAggregatedStatsByDimension] = new ConcurrentLinkedQueue[SessionsAggregatedStatsByDimension]()

  override def invoke(value: SessionsAggregatedStatsByDimension, context: SinkFunction.Context): Unit = {
    data.add(value)
  }
}


object MixedInMemorySinkFunction extends SinkFunction[AggregatedByName] {
  val data: ConcurrentLinkedQueue[AggregatedByName] = new ConcurrentLinkedQueue[AggregatedByName]()

  override def invoke(value: AggregatedByName, context: SinkFunction.Context): Unit = {
    data.add(value)
  }
}
