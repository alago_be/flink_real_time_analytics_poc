package com.boxever.analytics.operators

import com.boxever.analytics.jobs.session.stats.SessionStatsAggrJob.keySelector
import com.boxever.analytics.jobs.session.stats.{AggregatedSessionStats, SessionClosedStats, SessionStatsDimensionsSelector,
  SessionsAggregatedStatsByDimension,SessionStatsAggregator, SessionStatsProcessWindow}
import com.boxever.analytics.prettyPrint
import monocle.macros.GenLens
import org.apache.flink.api.common.typeinfo.TypeInformation
import org.apache.flink.streaming.util.KeyedOneInputStreamOperatorTestHarness
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers
import org.apache.flink.api.common.ExecutionConfig
import org.apache.flink.api.java.functions.KeySelector
import org.apache.flink.streaming.api.windowing.time.Time
import org.apache.flink.streaming.api.windowing.assigners.TumblingEventTimeWindows
import org.apache.flink.streaming.api.windowing.triggers.EventTimeTrigger
import org.apache.flink.streaming.api.windowing.windows.TimeWindow
import org.apache.flink.streaming.runtime.operators.windowing.WindowOperatorBuilder
import org.apache.flink.streaming.api.scala.function.util.ScalaProcessWindowFunctionWrapper

import java.util.concurrent.TimeUnit
import scala.jdk.CollectionConverters.collectionAsScalaIterableConverter



class SessionStatsTest extends AnyFlatSpec with Matchers {

  val sessions = Seq(
    SessionClosedStats("pqsPERS3lw12v5a9rrHPW1c4hET73GxQ",1L,"SESSION","9b4d4ab2-5a49-4361-9af4-93392a782410",1639651698595L,"CLOSED","spinair.com","WEB","Unknown","Downloading Tool",1639651698560L,1639651701020L,1,"home page","https://spinair.boxever.io/"),
    SessionClosedStats("pqsPERS3lw12v5a9rrHPW1c4hET73GxQ",2L,"SESSION","88d703b6-55f5-4dd2-90ce-97b146931d24",1639651711844L,"CLOSED","spinair.com","WEB","Unknown","Downloading Tool",1639651711790L,1639651727605L,0,"home page","https://spinair.boxever.io/"),
    SessionClosedStats("pqsPERS3lw12v5a9rrHPW1c4hET73GxQ",3L,"SESSION","33e10227-a8f3-47fc-b00c-6a7bad2b7108",1639651758216L,"CLOSED","spinair.com","WEB","Unknown","Downloading Tool",1639651758177L,1639651759179L,0,"home page",null),
    SessionClosedStats("pqsPERS3lw12v5a9rrHPW1c4hET73GxQ",3L,"SESSION","db8237ee-46b4-49b6-8004-1e18ec1edb92",1639651774234L,"CLOSED","spinair.com","WEB","Unknown","Downloading Tool",1639651774177L,1639651775827L,0,null,null),
    SessionClosedStats("pqsPERS3lw12v5a9rrHPW1c4hET73GxQ",4L,"SESSION","bace61f2-2683-4c77-a5f7-b297f94d95c7",1639650774593L,"CLOSED","spinachair.com","MOBILE_APP","iOS 5 (iPhone)","Mobile Safari",1639650774543L,1639650970160L,1,"/offers",null),
    SessionClosedStats("pqsPERS3lw12v5a9rrHPW1c4hET73GxQ",5L,"SESSION","338a9068-9312-4627-a400-95e91937325c",1639650975414L,"CLOSED","spinachair.com","WEB","Mac OS X","Chrome",1639650975378L,1639650980825L,1,"/",null),
    SessionClosedStats("pqsPERS3lw12v5a9rrHPW1c4hET73GxQ",10L,"SESSION","01c3abbd-1fd7-4a2a-a9b7-c98fa5adc63d",1639650986161L,"CLOSED","spinachair.com","MOBILE_APP","iOS 5 (iPhone)","Mobile Safari",1639650986117L,1639651006793L,1,"/",null),
    SessionClosedStats("pqsPERS3lw12v5a9rrHPW1c4hET73GxQ",20L,"SESSION","54e86b8d-055d-499d-8c20-d129566318c1",1639651012050L,"CLOSED","spinachair.com","MOBILE_WEB","Android 4.x Tablet","Opera 12",1639651012009L,1639651218222L,1,"/",null),
    SessionClosedStats("pqsPERS3lw12v5a9rrHPW1c4hET73GxQ",23L,"SESSION","28d07288-a627-4cbc-890f-f8f317c6d149",1639651223496L,"CLOSED","spinachair.com","WEB","Mac OS X","Chrome",1639651223443L,1639651424224L,3,"/",null)
  )

  val sessionModifyAt = GenLens[SessionClosedStats](_.modifiedAt)

  private var testHarness: KeyedOneInputStreamOperatorTestHarness[SessionStatsDimensionsSelector, SessionClosedStats, SessionsAggregatedStatsByDimension] = null

  // Trying this
  // https://github.com/apache/flink/blob/5e4efadcae3f89d0661470b754f920a3f0cae337/flink-streaming-java/src/test/java/org/apache/flink/streaming/runtime/operators/windowing/WindowOperatorTest.java#L108
  // as there is no predefined harnesses for AggregateFunctions
  "sessionStatsAggregator" should
    "aggregate" in {


    val config = new ExecutionConfig

    val processWindow = new ScalaProcessWindowFunctionWrapper(new SessionStatsProcessWindow())
    val operator =
      new WindowOperatorBuilder[SessionClosedStats,SessionStatsDimensionsSelector, TimeWindow](
      TumblingEventTimeWindows.of(Time.of(60, TimeUnit.MILLISECONDS)),
      EventTimeTrigger.create(),
      config,
      TypeInformation.of(classOf[SessionClosedStats]),
      { v => keySelector(v)},
      TypeInformation.of(classOf[SessionStatsDimensionsSelector])
    ).aggregate(new SessionStatsAggregator(), processWindow, TypeInformation.of(classOf[AggregatedSessionStats]))

    val selector = new KeySelector[SessionClosedStats, SessionStatsDimensionsSelector]() {
      override def getKey(value: SessionClosedStats): SessionStatsDimensionsSelector = keySelector(value)
    }

    testHarness = new KeyedOneInputStreamOperatorTestHarness[SessionStatsDimensionsSelector, SessionClosedStats, SessionsAggregatedStatsByDimension](
      operator,
      selector,
      TypeInformation.of(classOf[SessionStatsDimensionsSelector]),
    1,
    1,
    0
    )

    testHarness.open()

    testHarness.processWatermark(0)
    sessions.foreach( s => testHarness.processElement(s,s.modifiedAt))
    testHarness.processWatermark(60L)

    sessions.foreach( s => {
      val newSession = sessionModifyAt.modify( _ + 60L)(s)
      testHarness.processElement(newSession,newSession.modifiedAt)
    })

    testHarness.processWatermark(120L)

    val out = testHarness.extractOutputStreamRecords().asScala.filterNot(_.isWatermark).toList
    println(prettyPrint(out))
    out.size should be (4)

    val first_element = out.head.getValue
    first_element.windowTime should be (59)
    first_element.dimensions.clientKey should be ("pqsPERS3lw12v5a9rrHPW1c4hET73GxQ")
    first_element.dimensions.pointOfSale should be ("spinair.com")
    first_element.stats.closed should be (4)
    first_element.stats.bounced should be (1)
    first_element.stats.pageViews should be (1)
    first_element.stats.refererHist should contain allElementsOf Map(
      "https://spinair.boxever.io/" -> 2
    )


    val second_element = out(1).getValue
    second_element.windowTime should be (59)
    second_element.dimensions.clientKey should be ("pqsPERS3lw12v5a9rrHPW1c4hET73GxQ")
    second_element.dimensions.pointOfSale should be ("spinachair.com")
    second_element.stats.closed should be (5)
    second_element.stats.bounced should be (4)
    second_element.stats.pageViews should be (7)
    second_element.stats.userAgentHist should contain allElementsOf Map(
      "Mobile Safari" -> 2,
      "Chrome" -> 2,
      "Opera 12" -> 1
    )
    second_element.stats.osHist should contain allElementsOf Map(
      "iOS 5 (iPhone)" -> 2,
      "Mac OS X" -> 2,
      "Android 4.x Tablet" -> 1
    )
    second_element.stats.firstPageHist should contain allElementsOf Map(
      "/offers" -> 1,
      "/" -> 4
    )

    val third_element = out(2).getValue
    third_element.windowTime should be (119)
    third_element.dimensions should be eq(first_element.dimensions)
    third_element.stats should be eq (first_element.stats)

    val forth_element = out(3).getValue
    forth_element.windowTime should be (119)
    forth_element.dimensions should be eq(second_element.dimensions)
    forth_element.stats should be eq(second_element.stats)




  }
}


