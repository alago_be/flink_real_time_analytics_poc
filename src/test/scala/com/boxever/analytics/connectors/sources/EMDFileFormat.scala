package com.boxever.analytics.connectors.sources

import com.boxever.analytics.models.{EMDHolderDeserializationSchema, ObjectMapperV2}
import com.boxever.kafka.model.v2.EntityModifiedDocumentMessage
import com.boxever.models.v2.Entity
import org.apache.flink.api.common.typeinfo.TypeInformation
import org.apache.flink.configuration.Configuration
import org.apache.flink.connector.file.src.reader.{FileRecordFormat, SimpleStreamFormat, StreamFormat}
import org.apache.flink.core.fs.{FSDataInputStream, Path}
import org.apache.flink.util.FlinkRuntimeException

import java.io.{BufferedReader, FileInputStream, IOException, InputStreamReader}
import scala.io.Source

class EMDFileFormat extends FileRecordFormat[EMDHolder] {

  import java.io.BufferedReader

  override def createReader(config: Configuration, filePath: Path, splitOffset: Long, splitLength: Long): FileRecordFormat.Reader[EMDHolder] = {
    val reader: Source = Source.fromFile(filePath.getPath)
    new EMDFileFormat.Reader (reader)
  }

  override def isSplittable: Boolean = false
  override def getProducedType: TypeInformation[EMDHolder] = {
    TypeInformation.of(classOf[EMDHolder])
  }

  // ------------------------------------------------------------------------

  override def restoreReader(config: Configuration, filePath: Path, restoredOffset: Long, splitOffset: Long, splitLength: Long): FileRecordFormat.Reader[EMDHolder] = {
    val reader: Source = Source.fromFile(filePath.getPath)
    new EMDFileFormat.Reader (reader)
  }
}

object EMDFileFormat {
  /** The actual reader for the {@code TextLineFormat}. */
  class Reader(val reader: Source) extends FileRecordFormat.Reader[EMDHolder] {
    val lines =  reader.getLines().toList.map( l => EMDHolderDeserializationSchema.deserialize(l.getBytes))
      .sortBy(_.message.asInstanceOf[EntityModifiedDocumentMessage[_]].getEntity.asInstanceOf[Entity].getModifiedAt).iterator
    @throws[IOException]
    override def read: EMDHolder= {
      if( lines.hasNext) {
        lines.next()
      } else {
        null
      }
    }

    @throws[IOException]
    override def close(): Unit = {
      reader.close()
    }
  }
}