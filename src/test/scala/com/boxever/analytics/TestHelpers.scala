package com.boxever.analytics

import com.boxever.analytics.connectors.sinks.{HistType, MapExtensions}
import com.boxever.analytics.connectors.sources.{EMDHolder, GuestEMDHolder, SessionEMDHolder}
import com.boxever.kafka.model.v2.BaseEntityMessage.EntityType
import org.apache.flink.api.common.functions.AggregateFunction
import org.apache.flink.streaming.api.scala.function.ProcessWindowFunction
import org.apache.flink.streaming.api.windowing.windows.TimeWindow
import org.apache.flink.util.Collector

import scala.collection.mutable

object TestHelpers {

  case class SimpleAggregate(counter: HistType)

  case class AggregatedByName(aggr: SimpleAggregate, name: String)

  class SimpleAggrFunction extends AggregateFunction[EMDHolder, SimpleAggregate, SimpleAggregate] {
    override def createAccumulator(): SimpleAggregate = SimpleAggregate(mutable.Map[String, Long]())

    override def add(value: EMDHolder, accumulator: SimpleAggregate): SimpleAggregate = {
      SimpleAggregate(accumulator.counter.inc(value._type match {
        case EntityType.GUEST => value.asInstanceOf[GuestEMDHolder].message.getGuest.getGuestType
        case EntityType.SESSION => value.asInstanceOf[SessionEMDHolder].message.getSession.getSessionType
      })
      )
    }

    override def getResult(accumulator: SimpleAggregate): SimpleAggregate = {
      accumulator
    }

    override def merge(a: SimpleAggregate, b: SimpleAggregate): SimpleAggregate = {
      SimpleAggregate(a.counter.merged(b.counter))
    }
  }

  class SimpleProcessWindow extends ProcessWindowFunction[SimpleAggregate, AggregatedByName, String, TimeWindow] {
    override def process(key: String, context: Context, elements: Iterable[SimpleAggregate], out: Collector[AggregatedByName]): Unit = {
      out.collect(AggregatedByName(SimpleAggregate(elements.foldLeft(mutable.Map[String, Long]())((a, i) => a.merged(i.counter))), key))
    }
  }
}
