package com.boxever.analytics.models

import com.boxever.kafka.model.v2.BaseEntityMessage.EntityType
import com.boxever.models.v2.Guest
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

class EMDV2DeserializationSchemaTest extends AnyFlatSpec with Matchers{

  "EMDV2DeserializationSchemaTest" should "Deserialize a guest entity" in {
      val guestsFile = getClass.getResourceAsStream("/data/emd_23_12_2021_dev.txt")
      val message: String = scala.io.Source.fromInputStream(guestsFile).getLines.take(1).toList(0)

      val schema = new EMDV2DeserializationSchema()
      val emd = schema.deserialize(message.getBytes)

      emd.getType should be(EntityType.GUEST.toString)
      val guest = emd.getEntity.asInstanceOf[Guest]
      guest.getGuestType should be("visitor")

    }

  it should "Deserialize a whole topic" in {
    val guestsFile = getClass.getResourceAsStream("/data/emd_23_12_2021_dev.txt")
    val messages: List[String] = scala.io.Source.fromInputStream(guestsFile).getLines.toList
    val schema = new EMDV2DeserializationSchema()
    for (m <- messages) {

      val emd = schema.deserialize(m.getBytes)

      //emd.getType should be(EntityType.GUEST.toString)
      //val guest = emd.getEntity.asInstanceOf[Guest]
      //guest.getGuestType should be("visitor")
    }

  }



}
