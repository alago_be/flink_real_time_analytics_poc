package com.boxever.analytics.config

import com.boxever.analytics.connectors.sinks.jdbc.JDBCConnectionConfig
import com.boxever.analytics.connectors.sources.KafkaConfig
import com.boxever.analytics.prettyPrint
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.dataformat.javaprop.JavaPropsMapper
import com.fasterxml.jackson.module.scala.{DefaultScalaModule, ScalaObjectMapper}
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

import java.util
import java.util.Properties
import scala.jdk.CollectionConverters.mapAsJavaMapConverter

class ConfigTest extends AnyFlatSpec with Matchers{

  "Config" should
    "deserialise properties to GuestsTypeVisistAggrJobConfig" in {
      val app = new Properties()
      app.asInstanceOf[util.Hashtable[String, String]].putAll(
        Map("name" -> "TestApp",
          "outOfOrderness" -> "3500",
          "schema_name" -> "Schema",
          "closed_sessions_table" -> "closed_sessions_table"
        ).asJava)
      val kafka = new Properties( )
      kafka.asInstanceOf[util.Hashtable[String, String]].putAll(Map(
        "offset"->"earliest",
        "groupID"-> "real_time_guests_widget",
        "topic" -> "entity_modified_document",
        "bootstrap"-> "kafka_one.eu-west-1.dev.boxever.internal:9092"

      ).asJava)
      val jdbc = new Properties( )
      jdbc.asInstanceOf[util.Hashtable[String, String]].putAll( Map(
        "secret"->"mysecret",
        "region"-> "MyRegion"
      ).asJava)
      val props: Map[String, Properties] = Map(
        "application" -> app,
        "kafka"-> kafka,
        "jdbc"-> jdbc
      )

      val  mapper: JavaPropsMapper= new JavaPropsMapper
    val scalaMapper = new ObjectMapper() with ScalaObjectMapper
    scalaMapper.registerModule(DefaultScalaModule)
      println(props)
   val config =  AggrJobConfig(
      scalaMapper.convertValue(props("application"), classOf[ApplicationConf]),
      mapper.convertValue(props("kafka"), classOf[KafkaConfig]),
      mapper.convertValue(props("jdbc"), classOf[JDBCConnectionConfig])
    )

      println(prettyPrint(config))

    }



}
