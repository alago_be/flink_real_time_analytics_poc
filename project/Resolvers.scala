import sbt._

object Resolvers {

  lazy val localMaven =  "Local Maven Repository" at "file://"+Path.userHome+"/.m2/repository"
  lazy val boxSnapshots =  "Boxever Snapshots" at "https://nexus.boxever.com/nexus/content/repositories/boxever-snapshots"
  lazy val boxReleases =  "Boxever Releases" at "https://nexus.boxever.com/nexus/content/repositories/boxever-releases"
  lazy val binTray =  "Bintray sbt plugin releases" at "https://dl.bintray.com/sbt/sbt-plugin-releases/"
  lazy val jcenter =  Resolver.jcenterRepo
  lazy val defaultMaven =  DefaultMavenRepository


  lazy val allResolvers = Seq(localMaven, boxSnapshots, boxReleases, binTray, jcenter, defaultMaven)

  lazy val boxCredentials = (sys.env.get("BOXEVER_NEXUS_USERNAME"), sys.env.get("BOXEVER_NEXUS_PASSWORD")) match {
    case (Some(username), Some(password)) =>
      Credentials("Sonatype Nexus Repository Manager", "nexus.boxever.com", username, password)
    // Fallback to local env where we dont have these envs set
    case _ => Credentials(Path.userHome / ".ivy2" / ".credentials")
  }

}